import {Routing} from './routing.js';
require('../css/homepage.index.scss');
//Get questions and associated answers
// [GET]/api/questions
$.getJSON( Routing.generate('api_questions_get_collection'))
    .then(questions => {
        questions.forEach(question => {
            // [GET]/api/questions/{id}/answer
            $.getJSON(Routing.generate('api_questions_answer_get_subresource', { id: question.id } )).then(answer => {
                //Write question + answer results in DOM
                $('.starter-template').append('<h2 >' + question.content + ' <i class="fa fa-question"></i></h2>')
                    .append('<p class="lead">' + answer.content + ' <i class="fa fa-reply"></i></p>');
            })
        });
    });