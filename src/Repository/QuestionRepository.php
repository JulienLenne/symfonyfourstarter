<?php

namespace App\Repository;


use App\Entity\Question;
use Doctrine\ORM\EntityRepository;

class QuestionRepository extends EntityRepository
{
    public function findAllQuestionWithAnswer()
    {
        $queryBuilder = $this->_em->createQueryBuilder()
            ->select(['q','a'])
            ->from(Question::class, 'q')
            ->leftJoin('q.answer', 'a');
        $query = $queryBuilder->getQuery();
        return $query->getResult();
    }
}