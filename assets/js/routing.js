//get routes from generated file with php bin/console fos:js-routing:dump --format=json --target=public/js/fos_js_routes.json
//Routes are exposed by config file in config/packages/fos_js_routing.yaml and "expose" annotations from controllers
// You can see all routes with php bin/console debug:router
const routes = require('../../public/js/fos_js_routes.json');
import Routing from '../../vendor/friendsofsymfony/jsrouting-bundle/Resources/public/js/router.min.js';
Routing.setRoutingData(routes);

export {Routing};