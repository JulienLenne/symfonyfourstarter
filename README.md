# Symfony4 starter kit with Encore(Webpack), SCSS, Bootstrap3, Jquery, FontAwesome #

This README document whatever steps are necessary to get your SF4 application up and running.

### What is this repository for? ###

* Quick & easy installation of SF4 Website
* Include most of the common and professional front-end modules

### How do I get set up? ###

* Install NodeJS
* Install NPM
* Install YARN
* Install PHP 7.1
* Install Composer
* Clone this repository: git clone git@bitbucket.org:JulienLenne/symfonyfourstarter.git your_project_name
* Install composer packages: cd your_project_name; composer install
* Install NodeJS package(See: composer.json): yarn install
* Launch YARN dev-server: yarn encore dev-server
* Open new terminal and start PHP dev-server: php bin/console server:run
* Go to http://localhost:8000/ and enjoy HOT RELOAD with manifest (you can change something in assets/css/app.scss no need to refresh)

### How it work? ###

* Open webpack.config.js and see the comments
* Every JS and SCSS vendors are loaded by Webpack from node_modules
* Manifest is created by Webpack and loaded by SF4 (see: config/packages/framework.yaml)
* Webpack load assets/js/main.js and preprocess assets/css/main.scss
* Base template is in templates/base.html.twig
* Controller for route / is in src/Controller/HomepageController.php
* Template for action / is in templates/homepage/index.html.twig and include Bootstrap3 starter kit

### Who do I talk to? ###

* Julien Lenne <contact.lenne@gmail.com>
* Skype: ockonor
