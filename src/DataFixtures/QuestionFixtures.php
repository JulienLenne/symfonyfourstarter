<?php

namespace App\DataFixtures;

use App\Entity\Answer;
use App\Entity\Question;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class QuestionFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $question = new Question;
        $question->setContent('What is the common sentence used to learn PHP');
        $answer = new Answer();
        $answer->setContent('Hello World!');
        $manager->persist($answer);
        $question->setAnswer($answer);
        $manager->persist($question);
        $manager->flush();

        $question = new Question;
        $question->setContent('What is the best PHP Framework');
        $answer = new Answer();
        $answer->setContent('No one is the best, depend on the usage needed');
        $manager->persist($answer);
        $question->setAnswer($answer);
        $manager->persist($question);
        $manager->flush();

        $question = new Question;
        $question->setContent('What else');
        $answer = new Answer();
        $answer->setContent('I don\'t Know');
        $manager->persist($answer);
        $question->setAnswer($answer);
        $manager->persist($question);
        $manager->flush();
    }
}
