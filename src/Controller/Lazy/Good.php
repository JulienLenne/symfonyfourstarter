<?php

namespace App\Controller\Lazy;

use App\Entity\Question;
use Symfony\Bridge\Doctrine\RegistryInterface;
use Symfony\Component\HttpFoundation\Response;
use Twig\Environment;

/**
 * Class Good
 * @package App\Controller\Lazy
 */
final class Good
{
    /**
     * @var RegistryInterface $orm
     */
    private $orm;

    /**
     * @var Environment $templating
     */
    private $templating;

    /**
     * Good constructor.
     * @param RegistryInterface $orm
     * @param Environment $templating
     */
    public function __construct(RegistryInterface $orm, Environment $templating)
    {
        $this->orm = $orm;
        $this->templating = $templating;
    }

    /**
     * @return Response
     * @throws \Twig_Error_Loader
     * @throws \Twig_Error_Runtime
     * @throws \Twig_Error_Syntax
     */
    public function __invoke()
    {
        $questions = $this->orm->getRepository(Question::class)->findAllQuestionWithAnswer();
        return new Response($this->templating->render('lazy/bad.html.twig', compact('questions')));
    }
}
